import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  private baseUrl = 'http://168.63.99.42/motos';
  products: any = [];
  precioProd: number;

  constructor(private http: HttpClient, private modalService: NgbModal) { }

  ngOnInit() {
    this.get_products();
  }

  get_products() {
    this.http.get(this.baseUrl).subscribe((res) => {
        this.products = res;
    });
  }

  calcPrice(pid, fecha, precio) {
    const total: number = 0;
    const today = new Date();
    const fecha1;

    if (pid === "MOTO_B") {
      fecha1 = moment(fecha, 'YYYY-DD-MM').format('YYYY-MM-DD');
    } else {
      fecha1 = moment(fecha);
    }

    const fecha2 = moment(today);
    const date = fecha2.diff(fecha1, 'years');

    total = precio;

    for (let i = 0; i < date; i++) {
      total = total / 2;
    }
    total = Math.round(total * 100) / 100;

    return total;
  }

  openModal(content, pid, fecha, precio) {
    this.precioProd = this.calcPrice(pid, fecha, precio);
    this.modalService.open(content, { centered: true });
  }

}
